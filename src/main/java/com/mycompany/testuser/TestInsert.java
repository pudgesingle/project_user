/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testuser;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author PlugPC
 */
public class TestInsert {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "INSERT INTO user (username, password, tel, name, surname, address, idnumber, salary, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            User user = new User(-1,"minimini", "plug1234","0907704532","rawiphat", "Thammachart", "359/50", "1239900284843", 300, 1);
            stmt.setString(1,user.getUsername());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getTel());
            stmt.setString(4, user.getName());
            stmt.setString(5, user.getSurname());
            stmt.setString(6, user.getAddress());
            stmt.setString(7, user.getIdnumber());
            stmt.setDouble(8, user.getSalary());
            stmt.setInt(9, user.getType());
            int row = stmt.executeUpdate();
            System.out.println("Affect row"+row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row"+row+" id: "+id);
            
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
