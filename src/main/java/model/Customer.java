/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author PlugPC
 */
public class Customer {
    private int id;
    private String name;
    private String surname;
    private String tel;
    private int point;

    public Customer(int id, String name, String surname, String tel, int point) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.tel = tel;
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getTel() {
        return tel;
    }

    public int getPoint() {
        return point;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", tel=" + tel + ", point=" + point + '}';
    }
    
    
}
