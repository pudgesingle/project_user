/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author PlugPC
 */
public class User {
    private int id;
    private String username;
    private String password;
    private String tel;
    private String name;
    private String surname;
    private String address;
    private String idnumber;
    private double salary;
    private int type;
    
    public User(int id, String username, String password, String tel, String name, String surname, String address, String idnumber, double salary, int type){
        this.id = id;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.idnumber = idnumber;
        this.salary = salary;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public double getSalary() {
        return salary;
    }

    public int getType() {
        return type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", password=" + password + ", tel=" + tel + ", name=" + name + ", surname=" + surname + ", address=" + address + ", idnumber=" + idnumber + ", salary=" + salary + ", type=" + type + '}';
    }
    
}
