package ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import dao.UserDao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Zerocool
 */
public class UserService {
    private static ArrayList<User> userList;

    public static ArrayList<User> getList() {
        return userList;
    }

    public static void load() {
        UserDao dao = new UserDao();
        userList = dao.getAll();
    }


    public static boolean auth(String username, String password) {
        for (User user : userList) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password))return true;
        }
        return false;
    }
    
}
