/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Stock;

/**
 *
 * @author 59161111
 */
public class StockDao implements DaoInterface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO stock (name, qty, unit, buy_price, qty_subunit, subunit) VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getQty());
            stmt.setString(3, object.getUnit());
            stmt.setDouble(4, object.getBuy_price());
            stmt.setInt(5, object.getQty_subunit());
            stmt.setString(6, object.getSubunit());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cann't add product");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,qty,unit,buy_price,qty_subunit,subunit FROM stock;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int qty = result.getInt("qty");
                String unit = result.getString("unit");
                double buy_price = result.getDouble("buy_price");
                int qty_subunit = result.getInt("qty_subunit");
                String subunit = result.getString("subunit");
             
                Stock stock = new Stock(id,name,qty,unit,buy_price,qty_subunit,subunit);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cann't get all Stock");
        }
        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
         try {
            String sql = "SELECT id,name,qty,unit,buy_price,qty_subunit,subunit FROM stock WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("id");
                String name = result.getString("name");
                int qty = result.getInt("qty");
                String unit = result.getString("unit");
                double buy_price = result.getDouble("buy_price");
                int qty_subunit = result.getInt("qty_subunit");
                String subunit = result.getString("subunit");
                
                Stock stock = new Stock(pid,name,qty,unit,buy_price,qty_subunit,subunit);
                return stock;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cann't get  stock");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM stock WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Cann't delete  Stock");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE stock SET name = ?,qty = ?,unit = ?,buy_price = ?,qty_subunit = ?,subunit = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getQty());
            stmt.setString(3, object.getUnit());
            stmt.setDouble(4, object.getBuy_price());
            stmt.setInt(5, object.getQty_subunit());
            stmt.setString(6, object.getSubunit());
            stmt.setInt(7, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Cann't update product");
        }
        db.close();
        return row;  }

    public static void main(String[] args) {
        StockDao dao = new StockDao();
        int id = dao.add(new Stock(-1, "มาม่า", 10, "กล่อง", 80.0, 20, "ซอง"));
        System.out.println("id: " + id);
        System.out.println(dao.getAll());
        System.out.println(dao.get(3));
        Stock lastStock = dao.get(id);
        System.out.println("lastStock " + lastStock);

        lastStock.setBuy_price(1000);
        dao.update(lastStock);
        Stock updateStock = dao.get(id);
        System.out.print("updateStock " + updateStock);
        
        dao.delete(id);
         Stock deleteStock = dao.get(id);
        System.out.print("deleteStock " + deleteStock);
    }

   
}
