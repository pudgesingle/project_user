/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.testuser.Test;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.runtime.Debug.id;
import model.User;

/**
 *
 * @author PlugPC
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO user (username, password, tel, name, surname, address, idnumber, salary, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getName());
            stmt.setString(5, object.getSurname());
            stmt.setString(6, object.getAddress());
            stmt.setString(7, object.getIdnumber());
            stmt.setDouble(8, object.getSalary());
            stmt.setInt(9, object.getType());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT id, username, password, tel, name, surname, address, idnumber, salary, type FROM user";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String username = result.getString("username");
                String password = result.getString("password");
                String tel = result.getString("tel");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String address = result.getString("address");
                String idnumber = result.getString("idnumber");
                double salary = result.getDouble("salary");
                int type = result.getInt("type");
                User user = new User(id, username, password, tel, name, surname, address, idnumber, salary, type);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT id, username, password, tel, name, surname, address, idnumber, salary, type FROM user WHERE id=" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String username = result.getString("username");
                String password = result.getString("password");
                String tel = result.getString("tel");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String address = result.getString("address");
                String idnumber = result.getString("idnumber");
                double salary = result.getDouble("salary");
                int type = result.getInt("type");
                User user = new User(uid, username, password, tel, name, surname, address, idnumber, salary, type);
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM user WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET username = ?, password = ?, tel = ?, name = ?, surname = ?, address = ?, idnumber = ?, salary = ?, type = ? WHERE id = ? ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getName());
            stmt.setString(5, object.getSurname());
            stmt.setString(6, object.getAddress());
            stmt.setString(7, object.getIdnumber());
            stmt.setDouble(8, object.getSalary());
            stmt.setInt(9, object.getType());
            stmt.setInt(10, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

}
